package com.mysoft.quartz.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mysoft.quartz.service.UserService;

/**
 * 参考：http://www.cnblogs.com/LiuChunfu/p/5598610.html
 */
@Component(value = "myJob2")
public class MyJob2 {
	
	@Autowired
	private UserService us;
	
	public void doSomething() {
		System.err.println("job2");
		us.sayHello();
	}
}
