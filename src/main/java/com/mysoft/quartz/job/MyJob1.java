package com.mysoft.quartz.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.mysoft.quartz.service.UserService;

/**
 * 参考：http://www.cnblogs.com/LiuChunfu/p/5598610.html
 */
public class MyJob1 extends QuartzJobBean {
	
	@Autowired
	private UserService userService;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		System.out.println("MyJob1");
		
		SchedulerContext schCtx = null;
		try {
			schCtx = context.getScheduler().getContext();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		ApplicationContext appCtx = (ApplicationContext)schCtx.get("applicationContext");
		userService= (UserService)appCtx.getBean("userService");
		
		userService.sayHello();
		
		
		
	}

}
