package com.mysoft.quartz.service.impl;

import org.springframework.stereotype.Service;

import com.mysoft.quartz.service.UserService;

@Service(value = "userService")
public class UserServiceImpl implements UserService {

	@Override
	public void sayHello() {
		System.out.println("hello");
	}

}
