package com.mysoft.quartz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysoft.quartz.job.ResetJob;

@Controller
public class MysoftController {

	@Autowired
	private ResetJob resetJob;
	
	String className = "com.mysoft.quartz.job.MyJob1";
	String cronExpression = "0/3 * * * * ?";
	
	@RequestMapping(value = "add")
	@ResponseBody
	public String add() throws Exception{
		
		resetJob.addJob(className, cronExpression);
		
		return "{\"result\":\"success\"}";
	}
	
	@RequestMapping(value = "pause")
	@ResponseBody
	public String pause() throws Exception{
		
		resetJob.pause(className);
		
		return "{\"result\":\"success\"}";
	}
	
	@RequestMapping(value = "resume")
	@ResponseBody
	public String resume() throws Exception{
		
		resetJob.resume(className);
		
		return "{\"result\":\"success\"}";
	}
	
	@RequestMapping(value = "stop")
	@ResponseBody
	public String stop() throws Exception{
		
		resetJob.stop(className);
		
		return "{\"result\":\"success\"}";
	}
}
